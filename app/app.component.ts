import {Component} from '@angular/core';
import {KPICreator} from './kpi-creator/kpi-creator.component';
import {KPIValues} from './kpi-values/kpi-values.component';
import {kpis} from './kpi-creator/kpi.mock';
import {KPI} from './kpi-creator/kpi';

@Component({
    selector: 'my-app',    
    template: `
        <h1> KPI Calculation Engine using MathJS </h1>
        <kpi-creator [kpis]="kpis" ></kpi-creator>
        <kpi-values [kpis]="kpis" company="GASCO" period="Jun"></kpi-values>
    `,
    directives:[KPICreator, KPIValues]
})
export class AppComponent {

    kpis: KPI[] = kpis;
}