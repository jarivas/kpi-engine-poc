import {Component, OnInit, Input} from '@angular/core';
import {KPI} from './kpi';


@Component({
    selector:"kpi-creator",
    templateUrl: "/app/kpi-creator/kpi-creator.component.html"
})

export class KPICreator{

    @Input() kpis: KPI[]; 

    ngInit() {

    }

}