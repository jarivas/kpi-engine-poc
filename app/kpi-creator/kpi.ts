export class KPI {
    id: string;
    name: string;
    unit: string;
    type: string; 
    formula: string; 
}