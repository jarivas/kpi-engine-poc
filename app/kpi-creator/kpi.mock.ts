export const kpis = [
        {"id":"kpi_001",
         "name":"Number of Incidents",
         "unit": "#",
         "type": "INPUT",
         "formula":""},
         {
         "id":"kpi_002",
         "name":"Million man hours",
         "unit": "MMH",
         "type": "INPUT",
         "formula":""},
         {"id":"kpi_003",
         "name":"Incident Ratio",
         "unit": "incidents/MMH",
         "type": "OUTPUT",
         "formula":"'Number of Incidents' / 'Million man hours'"}
    ];