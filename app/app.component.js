System.register(['@angular/core', './kpi-creator/kpi-creator.component', './kpi-values/kpi-values.component', './kpi-creator/kpi.mock'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, kpi_creator_component_1, kpi_values_component_1, kpi_mock_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (kpi_creator_component_1_1) {
                kpi_creator_component_1 = kpi_creator_component_1_1;
            },
            function (kpi_values_component_1_1) {
                kpi_values_component_1 = kpi_values_component_1_1;
            },
            function (kpi_mock_1_1) {
                kpi_mock_1 = kpi_mock_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                    this.kpis = kpi_mock_1.kpis;
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n        <h1> KPI Calculation Engine using MathJS </h1>\n        <kpi-creator [kpis]=\"kpis\" ></kpi-creator>\n        <kpi-values [kpis]=\"kpis\" company=\"GASCO\" period=\"Jun\"></kpi-values>\n    ",
                        directives: [kpi_creator_component_1.KPICreator, kpi_values_component_1.KPIValues]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map