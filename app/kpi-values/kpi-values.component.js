System.register(['@angular/core', './kpi-actuals'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, kpi_actuals_1;
    var KPIValues;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (kpi_actuals_1_1) {
                kpi_actuals_1 = kpi_actuals_1_1;
            }],
        execute: function() {
            KPIValues = (function () {
                function KPIValues() {
                    this.scope = {};
                }
                KPIValues.prototype.translateFormula = function (formula) {
                    var re = /\'(.*?)\'/g;
                    var m;
                    var translatedFormula = formula;
                    while ((m = re.exec(formula)) !== null) {
                        if (m.index === re.lastIndex) {
                            re.lastIndex++;
                        }
                        var k = this.kpis.find(function (e) { return e.name == m[0].substring(1, m[0].length - 1); });
                        translatedFormula = translatedFormula.replace(m[0], k.id);
                    }
                    return translatedFormula;
                };
                KPIValues.prototype.ngOnInit = function () {
                    var _this = this;
                    this.kpiActuals = new Array();
                    this.kpis.forEach(function (element) {
                        var k = new kpi_actuals_1.KPIActual();
                        var pos = _this.kpiActuals.length;
                        k.kpi = element;
                        k.company = _this.company;
                        k.period = _this.period;
                        k.value = null;
                        console.log(k);
                        _this.kpiActuals.push(k);
                        // add the value to the scope if this is an input KPI -- not required
                        if (element.type == 'INPUT') {
                            _this.scope[element.id] = _this.kpiActuals[pos].value;
                        }
                    });
                };
                KPIValues.prototype.onChange = function (actual) {
                    var _this = this;
                    // update scope
                    this.scope[actual.kpi.id] = actual.value;
                    // evaluate formulas dynamically
                    this.kpiActuals.forEach(function (element) {
                        if (element.kpi.type == 'OUTPUT') {
                            element.value = math.eval(_this.translateFormula(element.kpi.formula), _this.scope);
                        }
                    });
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], KPIValues.prototype, "kpis", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], KPIValues.prototype, "company", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], KPIValues.prototype, "period", void 0);
                KPIValues = __decorate([
                    core_1.Component({
                        selector: "kpi-values",
                        templateUrl: "/app/kpi-values/kpi-values.component.html"
                    }), 
                    __metadata('design:paramtypes', [])
                ], KPIValues);
                return KPIValues;
            }());
            exports_1("KPIValues", KPIValues);
        }
    }
});
//# sourceMappingURL=kpi-values.component.js.map