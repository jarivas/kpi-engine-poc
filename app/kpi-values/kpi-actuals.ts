import {KPI} from '../kpi-creator/kpi'

export class KPIActual {
    kpi: KPI;
    period: string;
    company: string;
    value: number; 
}