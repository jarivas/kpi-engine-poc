import {Component, OnInit, Input} from '@angular/core';
import {KPIActual} from './kpi-actuals';
import {KPI} from '../kpi-creator/kpi'


@Component({
    selector:"kpi-values",
    templateUrl: "/app/kpi-values/kpi-values.component.html"
})

export class KPIValues{

    @Input() kpis: KPI[];
    @Input() company: string;
    @Input() period: string;  

    kpiActuals: KPIActual[];

    scope = {};

    translateFormula(formula:string) {
        var re = /\'(.*?)\'/g; 
        var m;
        var translatedFormula = formula;

        while ((m = re.exec(formula)) !== null) {
            if (m.index === re.lastIndex) {
                re.lastIndex++;
            }
            var k = this.kpis.find(e => {return e.name == m[0].substring(1,m[0].length-1)});
            translatedFormula = translatedFormula.replace(m[0],k.id);
        }
        return translatedFormula;
    }

    ngOnInit() {        
        this.kpiActuals = new Array<KPIActual>();

        this.kpis.forEach(element => {
            let k = new KPIActual();
            let pos = this.kpiActuals.length; 
            k.kpi = element;
            k.company = this.company;
            k.period = this.period;
            k.value = null;
            console.log(k);
            this.kpiActuals.push(k);
            // add the value to the scope if this is an input KPI -- not required
            if (element.type == 'INPUT') {
                this.scope[element.id] = this.kpiActuals[pos].value;
            }

        });
    }

    onChange(actual:KPIActual) {
        // update scope
        this.scope[actual.kpi.id] = actual.value;
        
        // evaluate formulas dynamically
        this.kpiActuals.forEach(element => {
            if (element.kpi.type == 'OUTPUT') {
                element.value = math.eval(this.translateFormula(element.kpi.formula),this.scope);
            }
        });
    }

}